# to be run from project dir ../

library(picar)

# read json files with items
lf <- list.files("data/vd18", pattern="records.json", recursive=T, full.names=T)
lf <- lf[grepl("vlo", lf)]
items <- sapply(lf, get_data, simplify=F)
items <- unlist(items, recursive=F)
names(items) <- gsub(".*\\.json\\.","", names(items))

# exclude authority files (persons, corporates)
items <- gbv_filter_bibtype(items, "A|O")

# collect data from items
identifiers <- sapply(items, vd18_number)
titles <- sapply(items, gbv_maintitle)
titles[sapply(titles, function(x) identical(x, character(0)))] <- ""
publishers <- sapply(items, gbv_publisher, collapse=TRUE)
publishers[sapply(publishers, function(x) identical(x, NULL))] <- ""
publishers <- gsub("^ | $","", publishers)
places <- sapply(items, gbv_place, collapse=TRUE)
places[sapply(places, function(x) identical(x, NULL))] <- ""
printers <- sapply(items, adr_printer, collapse=TRUE)
printers[sapply(printers, function(x) identical(x, NULL))] <- ""
printers <- gsub("^ | $","", printers)
# printers <- gsub("|","", printers, fixed=T)
printers_gnd <- sapply(items, adr_printer_gnd, collapse=TRUE)
printers_gnd[sapply(printers_gnd, function(x) identical(x, NULL))] <- ""
printers_gnd <- gsub("gnd/", "", printers_gnd, fixed=T)
dates <- sapply(items, gbv_year)
dates[sapply(dates, function(x) identical(x, NULL))] <- ""
types <- sapply(items, gbv_bibtype)

# create data frame
df <- data.frame(ppn=names(titles), vd18_nr=as.character(identifiers), type=as.character(types),
                 printer=as.character(printers), printer_gnd=as.character(printers_gnd),
                 publisher=as.character(publishers), place=as.character(places),
                 date=as.character(dates), title=as.character(titles), stringsAsFactors=FALSE)
df <- df[!duplicated(df), ]
df <- df[order(df$date, df$type),]

# save as csv file
write.csv(df, file="view/output/vd18k10p--any--all.csv", row.names=F)

# save item subsets as csv files
printed_any <- df[grepl("A", df$type),]
write.csv(printed_any, file="view/output/vd18k10p--any--printed.csv", row.names=F)
online_any <- df[grepl("O", df$type),]
write.csv(online_any, file="view/output/vd18k10p--any--online.csv", row.names=F)
printed_mono <- df[grepl("Aa", df$type),]
write.csv(printed_mono, file="view/output/vd18k10p--mono--printed.csv", row.names=F)
online_mono <- df[grepl("Oa", df$type),]
write.csv(online_mono, file="view/output/vd18k10p--mono--online.csv", row.names=F)
printed_journal <- df[grepl("Ab", df$type),]
write.csv(printed_journal, file="view/output/vd18k10p--journal--printed.csv", row.names=F)
online_journal <- df[grepl("Ob", df$type),]
write.csv(online_journal, file="view/output/vd18k10p--journal--online.csv", row.names=F)
printed_multi <- df[grepl("Ac", df$type),]
write.csv(printed_multi, file="view/output/vd18k10p--multi--printed.csv", row.names=F)
online_multi <- df[grepl("Oc", df$type),]
write.csv(online_multi, file="view/output/vd18k10p--multi--online.csv", row.names=F)
printed_part <- df[grepl("Af", df$type),]
write.csv(printed_part, file="view/output/vd18k10p--part--printed.csv", row.names=F)
online_part <- df[grepl("Of", df$type),]
write.csv(online_part, file="view/output/vd18k10p--part--online.csv", row.names=F)
