# PICA-Daten für das Hallesche Druckwesen

## Datenquellen

### CERL – Heritage of the Printed Book Database

> The HPB Database (previously called the Hand Press Book Database) is a steadily growing collection of files of catalogue records from major European and North American research libraries covering items of European printing of the hand-press period (c.1455-c.1830) integrated into one file. ... The HPB Database is of interest to librarians and any one else with academic pursuits across many fields of study that use printed books as source material. It is especially valuable for research in intellectual history, social history, and transmission of thought – as well as in the history of printing and the history of the book. ([Quelle](https://gso.gbv.de/DB=1.77/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/hpb
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.plc=Halle](http://sru.gbv.de/hpb?version=2.0&operation=searchRetrieve&query=pica.plc%3DHalle&recordSchema=picaxml&maximumRecords=5), [pica.pub=Halle](http://sru.gbv.de/hpb?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle&recordSchema=picaxml&maximumRecords=5)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/hpb) vom GBV.

### VD 17 – Verzeichnis der im deutschen Sprachraum erschienenen Drucke des 17. Jahrhunderts ([DE-627-4](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627-4))

> Das Verzeichnis der im deutschen Sprachraum erschienenen Drucke des 17. Jahrhunderts (VD 17) ist eine retrospektive Nationalbibliographie für den Zeitraum von 1601 bis 1700. Erfasst werden alle deutschsprachigen Drucke und alle im historischen deutschen Sprachgebiet gedruckte und verlegte Werke, unabhängig von ihrer Sprache. ([Quelle](http://www.vd17.de/))

_Search and Retrieve via URL_

- Dokumentation: https://wiki.k10plus.de/display/K10PLUS/SRU
- Endpunkt: http://sru.k10plus.de/vd17
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfrage: [pica.vlo=Halle](http://sru.k10plus.de/vd17?version=2.0&operation=searchRetrieve&query=pica.vlo%3DHalle&recordSchema=picaxml&maximumRecords=5)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/vd17) vom GBV.

### VD 18 – Verzeichnis Deutscher Drucke des 18. Jahrhunderts ([DE-627-5](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627-5))

> In diesem "Verzeichnis der im deutschen Sprachraum erschienenen Drucke des 18. Jahrhunderts" werden alle zwischen 1701 und 1800 in deutscher Sprache oder im deutschen Sprachraum erschienenen Drucke kooperativ erfasst und mit einer individuellen VD18-Nummer versehen. Derzeit (Stand: Februar 2018) enthält die von der Verbundzentrale des Gemeinsamen Bibliotheksverbunds betreute VD18-Datenbank rund 178.000 Monographien, 9.500 mehrbändige Werke mit 28.000 Bänden und ca. 3.300 Zeitschriftentitel. ([Quelle](https://gso.gbv.de/DB=1.65/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/vd18
    - jetzt auch über den K10plus: http://sru.k10plus.de/vd18
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.pub=Halle](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DHalle&recordSchema=picaxml&maximumRecords=10), [pica.plc=Halle](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.plc%3DHalle&recordSchema=picaxml&maximumRecords=10), [pica.vlo=Halle](http://sru.k10plus.de/vd18?version=2.0&operation=searchRetrieve&query=pica.vlo%3DHalle&recordSchema=picaxml&maximumRecords=10)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/vd18) vom GBV.

## Verwendete Software

- [Perl](https://www.perl.org/)
    - [Catmandu](https://metacpan.org/release/Catmandu) ([Handbook](https://librecat.org/Catmandu/))
        - [Catmandu::PICA](https://metacpan.org/release/Catmandu-PICA)
        - [Catmandu::SRU](https://metacpan.org/release/Catmandu-SRU)
